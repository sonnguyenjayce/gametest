using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animated : MonoBehaviour
{
    #region Prrivate
        private SpriteRenderer _spriteRenderer;
        private int _frame;
    #endregion
    #region Public
        public Sprite[] spript;
    #endregion
    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void OnEnable() 
    {
        Invoke ( nameof ( Animate ) , 0f );    
    }
    private void Animate()
    {
        _frame ++;
        if(_frame >= spript.Length)
        {
            _frame = 0;
        }
        if(_frame >= 0 && _frame < spript.Length)
        {
            _spriteRenderer.sprite = spript[_frame];
        }
        Invoke ( nameof ( Animate ) , 0.1f / GameManager.Instance.gameSpeed);
    }
}

