using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [System.Serializable]
    public struct spawnObject
    {
        public GameObject prefab;
        [Range(0f,1f)]
        public float spawChanner;
    }
    public spawnObject[] objecta;
    public float minSpawn = 1f;
    public float maxSpawn = 2f;
    private void OnEnable()
    {
        Invoke (nameof(Spawns), Random.Range(minSpawn,maxSpawn));
    }
    private void OnDisable()
    {
        CancelInvoke();
    }
    private void Spawns()
    {
        float spawChanner = Random.value;
        foreach(var obj in objecta)
        {
            if(spawChanner < obj.spawChanner)
            {
                GameObject Obstical = Instantiate(obj.prefab);
                Obstical.transform.position += transform.position;
                break;
            }
            spawChanner -= obj.spawChanner;
        }
        Invoke (nameof (Spawns), Random.Range(minSpawn, maxSpawn));
    }
}
