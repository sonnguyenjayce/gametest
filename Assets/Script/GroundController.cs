using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour
{
    #region Private
        private MeshRenderer _meshRenderer;
    #endregion
    #region Public
    #endregion
    private void Awake() 
    {
        _meshRenderer = GetComponent<MeshRenderer>();    
    }
    private void Update()
    {
        float speed = GameManager.Instance.gameSpeed / transform.localScale.x;
        _meshRenderer.material.mainTextureOffset -= Vector2.right * speed * Time.deltaTime;
    }
}
