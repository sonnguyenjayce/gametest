using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Private
        private CharacterController _character;
        private Vector3 _direction;
    #endregion
    #region Public
        public float gravity = 9.81f;
        public float JumpForce = 8f;
    #endregion
    private void Awake() 
    {
        _character = GetComponent<CharacterController>();  
    }
    private void OnEnable()
    {
        _direction = Vector3.zero;
    }
    private void Update()
    {
        _direction += Vector3.down * gravity * Time.deltaTime;
        if(_character.isGrounded)
        {
            _direction = Vector3.down;
            if(Input.GetButton("Jump"))
            {
                _direction = Vector3.up * JumpForce;
            }
        }
        _character.Move(_direction * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.CompareTag("Obsject"))
        {

        }
    }
}
