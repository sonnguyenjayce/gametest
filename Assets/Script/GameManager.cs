    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Private
        private PlayerController _player;
        private Spawn _spawn;
    #endregion
    #region Public
        public static GameManager Instance { get ; private set ; }
        public float gameSpeed { get ; private set ;}
        public float initialGameSpeed = 5f;
        public float gameSpeedIncrease = 0.1f;
    #endregion
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
    private void OnDestroy()
    {
        if(Instance == this)
        {
            Instance = null;
        }
    }
    private void Start() 
    {
        _player = FindObjectOfType<PlayerController>();
        _spawn = FindObjectOfType<Spawn>();
        NewGame();    
    }
    private void NewGame()
    {
        // Obstical[] Obstica = FindObjectOfType<Obstical>();

        gameSpeed = initialGameSpeed;
        enabled = true;
    }
    public void GameOver()
    {
        gameSpeed = 0f;
        enabled = false;
        _player.gameObject.SetActive(false);
        _spawn.gameObject.SetActive(false);
    }
}
